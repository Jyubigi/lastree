﻿using UnityEngine;
using System.Collections;

public class SMPlayer_JumpEnd : StateMachineBehaviour
{
    Player player = null;
    override public void OnStateExit(Animator _animator, AnimatorStateInfo _stateInfo, int _layerIndex)
    {
        if (!player)
            player = _animator.transform.parent.GetComponent<Player>();
        player.JumpEnd();
    }
}
