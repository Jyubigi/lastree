﻿using UnityEngine;
using System.Collections;

public class SmoothCam : MonoBehaviour {
    
    public Transform taget;
    public float DampSpeed = 10f;
    public Vector3 V3offset = Vector3.zero;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        Smooth_Cam();
	}
    void Smooth_Cam()
    {
        Vector3 _set = Vector3.Lerp(transform.position, taget.position, Time.deltaTime * DampSpeed);
        _set = _set - V3offset;
        transform.position = new Vector3(_set.x,_set.y, transform.position.z);
    }
}
