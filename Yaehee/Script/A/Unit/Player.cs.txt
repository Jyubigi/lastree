﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Spriter2UnityDX;
public class Player : Unit {

    #region Values
    public WeaponSlot wmanager;
    private bool isFR = true;

    private int hashJump_Start = 0;
    private int hashIdle = 0;
    private int hashWalk = 0;
    public Transform GroundChekerA= null;
    public Transform GroundChekerB = null;
    public Transform Sada = null;
    private float groundRad = 0.5f;

    public LayerMask whatIsGro;
    public LayerMask whatIsSadari;
    public TraceCam traceCam;

    private Vector2 temCollSize = new Vector3(0.5f, 1f);
    public PolygonCollider2D Poly = null;
    public CircleCollider2D Cir = null;
    public Rigidbody2D rigi = null;

    private bool isSadari = false;
    public bool isLR = true;
    private float ti = 0f;
    public EntityRenderer spr = null;
    #endregion

    #region get set
    #endregion

    #region Default Function
    // Use this for initialization
    void Awake()
    {
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        GroundCheck();
        Collider2D _sada = SadariCheck();
        anim.SetBool("isSadari", _sada);
        if (_sada)
        {
            Vector2 _cp = _sada.transform.position;
            if (_cp.y > transform.position.y)
                anim.SetBool("isClimb", true);
            else
                anim.SetBool("isClimb", false);
        }
        if(isLR)
           Move_LR();
    }
    void OnCollisionEnter2D(Collision2D _coll)
    {
        if (_coll.collider.tag.Equals("Weapon"))
        {
            wmanager.GetWeapon(_coll.collider.transform);// 무기 겟
        }
        if (_coll.collider.tag.Equals("Monster"))
        {
            anim.SetTrigger("Death");
        }
    }
    void OnCollisionExit2D(Collision2D _coll)
    {
    }
    #endregion

    #region Custom Function
   public void SadariEnter()
    {
        rigi.velocity = Vector2.zero;

        Vector2 _cpos = SadariCheck().transform.position;
        transform.position = new Vector3(_cpos.x
                                         , transform.position.y, transform.position.z);
        
        isLR = false;
        
        Poly.isTrigger = true;
        Cir.isTrigger = true;   
    }
    public void SadariExit()
    {
        StartCoroutine("SadariEnding");
        isLR = true;
        
        Poly.isTrigger = false;
        Cir.isTrigger = false;
    }
    void Move_LR()
    {
        // 좌우움직
        float _h = anim.GetFloat("StickX");
        if (_h < 0f && isFR)
        {
            Flip();
        }
        else if (_h > 0f && !isFR)
        {
            Flip();
        }
        rigi.velocity = new Vector2(_h * Speed, rigi.velocity.y);
    }
    
    void Flip()
    {
        isFR = !isFR;
        Vector3 Scale = transform.localScale;
        Scale.x *= -1;
        transform.localScale = Scale;
    }

    public void JumpStart()
    {
        
    }
    public void JumpEnd()
    {
        
    }
    void GroundCheck()
    {
        anim.SetBool("Grounded", Physics2D.OverlapArea(GroundChekerA.position,
                                                       GroundChekerB.position,
                                                       whatIsGro));
    }
    
    public Collider2D SadariCheck()
    {
        return Physics2D.OverlapPoint(Sada.position, whatIsSadari);
    }
    public void Death()
    {
        wmanager.Death();
        StartCoroutine("Dead");
    }
    public void Clear()
    {
        wmanager.Death();
    }
    #endregion

    #region Coroutine

    IEnumerator SadariEnding()
    {
        anim.SetBool("exitSadari", true);
        yield return new WaitForSeconds(1f);
        anim.SetBool("exitSadari", false);
    }
    IEnumerator Dead()
    {
        ti = 1f;
        while (ti > 0f)
        {
            spr.Color = new Color(1f, 1f, 1f, ti);
            ti -= Time.deltaTime *2f;
            yield return null;
        }
        traceCam.enabled = false;
        Poly.isTrigger = true;
        Cir.isTrigger = true;

        yield return new WaitForSeconds(1.5f);
        StartCoroutine("Rivival");
    }
    IEnumerator Rivival()
    {
        traceCam.enabled = true;
        Poly.isTrigger = false;
        Cir.isTrigger = false;
        MapDirector.Instance.LoadMap();
        transform.localPosition = Vector3.zero;
        ti = 0f;
        while (ti <= 1f)
        {
            spr.Color = new Color(1f, 1f, 1f, ti);
            ti += Time.deltaTime;
            yield return null;
        }
    }
    #endregion
}