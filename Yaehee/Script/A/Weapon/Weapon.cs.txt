﻿using UnityEngine;
using System.Collections;
/// <summary>
/// 무기의 기본 클래스
/// </summary>
public abstract class Weapon : MonoBehaviour
{
    #region Values
    public Unit unit = null;
    public Rigidbody2D rigi = null;
    public Collider2D coll = null;
    public int HitPerSec = 1;
    public int Life = 1;
    /// <summary>
    /// Power : 공격력은 던질떄 유닛에서 줘버려!
    /// </summary>
    public int Power = 1;
    public float Speed = 1f;
    public float ShootingDistance = 5f;
    public bool IsHand = false;

    public bool isThorw = false;
    private SpriteRenderer spr = null;
    #endregion
    protected abstract void Do_Attack(Vector2 _divec, float _chargeTime=0f);
    /// <summary>
    /// 무기를 손에 들었을때 실행되는 함수
    /// </summary>
    abstract protected void Hand();
    void Awake()
    {
        unit = GameObject.Find("Character").GetComponent<Player>();
        spr = transform.GetComponent<SpriteRenderer>();
    }
    /// <summary>
    /// 무기를 손에 들었을때 실행되는 함수
    /// </summary>
    public void OnHand()
    {
        rigi.isKinematic = true;// 물리효과를 사용하지 않도록 설정
        coll.isTrigger = true;//박스콜리더를 트리거로설정
        Hand();
        transform.localScale = Vector2.one;
    }
    public void Attack(Vector2 _divec, float _chargeTime = 0f)
    {
        Do_Attack(_divec, _chargeTime);
    }
    public void Throw()
    {
        transform.parent = GameObject.Find("Ingame").transform;
        rigi.isKinematic = false;
        rigi.AddForce(new Vector2(100f,10f));
        transform.localScale = Vector2.one;
        isThorw = true;
    }
    void OnTriggerEnter2D(Collider2D _coll)
    {
        if (isThorw && _coll.gameObject.tag.Equals("Ground"))
        {
            coll.isTrigger = false;
        }
    }
    public void SetVisible(bool _tf)
    {
        spr.color = new Color(1f, 1f, 1f, 1f * (float)System.Convert.ToInt32(_tf));
    }
}