﻿using UnityEngine;
using System.Collections;

public class Weapon_Auto : Weapon {
    public LayerMask WhatIsMob;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    protected override void Do_Attack(Vector2 _divec, float _chargeTime=0f)
    {
        Collider2D _mob = Physics2D.OverlapCircle(transform.position, ShootingDistance, WhatIsMob);
        if (_mob && !_mob.name.Equals(unit.name) && _mob.tag.Equals("Unit"))
        {
            _mob.transform.GetComponent<Unit>().Get_Damage(unit.Ap * Power);
            --Life;
        }
    }

    override protected void Hand()
    {
        // throw new System.NotImplementedException();
    }
}
