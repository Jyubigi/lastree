﻿using UnityEngine;
using System.Collections;

public class Weapon_Lazer : Weapon
 {
    public LayerMask WhatIsMob;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    protected override void Do_Attack(Vector2 _divec, float _chargeTime =0f)
    {
        Vector2 _direc = new Vector2(transform.localPosition.x + _divec.x, transform.localPosition.y + _divec.y);
        RaycastHit2D _hit = Physics2D.Raycast(transform.localPosition , _direc,ShootingDistance,WhatIsMob);
        if (_hit)
        {
            // 데미지연산
        }
    }
    override protected void Hand()
    {
    }
}
