﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 오브1 C급 무속성 투사체
/// 공격력3 초당공격횟수1
/// </summary>
public class Weapon_Throw : Weapon {
    public float ThrowPower = 1f;

	
	void Update () {
	}

    protected override void Do_Attack(Vector2 _divec, float _chargeTime = 0f)
    {
        isThorw = true;
        transform.parent = GameObject.Find("Ingame").transform;
        rigi.isKinematic = false;
        coll.isTrigger = true;
        rigi.AddForce(_divec * ThrowPower);
        --Life;
        transform.localScale = Vector2.one;
    }
    override protected void Hand()
    {
        isThorw = false;
    }
}